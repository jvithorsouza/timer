package timer.ucsal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChessTimer extends JFrame {
	private JLabel whiteLabel;
	private JLabel blackLabel;
	private Timer timer;
	private int whiteTime = 600; // tempo para as jogadas
	private int blackTime = 600; // tempo para as jogadas
	private boolean whiteTurn = true;
	private TimerThread timerThread;

	// private JLabel whiteLabel;
	// private JLabel blackLabel;
	private JPanel topPanel;
	private JPanel centerPanel;

	public ChessTimer() {
		super("Relógio de Xadrez");
		// paines para comportar todos os botoes e organizar relogios
		topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());

		JPanel clockPanel = new JPanel(new GridLayout(1, 2));
		whiteLabel = new JLabel(getTime(whiteTime));
		whiteLabel.setFont(new Font("Arial", Font.PLAIN, 50));
		whiteLabel.setHorizontalAlignment(JLabel.CENTER);

		blackLabel = new JLabel(getTime(blackTime));
		blackLabel.setFont(new Font("Arial", Font.PLAIN, 50));
		blackLabel.setHorizontalAlignment(JLabel.CENTER);
		clockPanel.add(blackLabel);
		clockPanel.add(whiteLabel);

		topPanel.add(clockPanel, BorderLayout.CENTER);

		add(topPanel, BorderLayout.NORTH);

		// botao das brancas
		JButton whitePassButton = new JButton("Brancas jogam.");
		whitePassButton.addActionListener(new ActionListener() {
			boolean timerStarted = false;

			public void actionPerformed(ActionEvent e) {

				if (!timerStarted) {
					timerStarted = true;
					timer.start();
				}

				timer.stop();

				whiteTurn = true;

				whiteLabel.setForeground(Color.RED);
				blackLabel.setForeground(Color.BLACK);

				timer.start();
			}
		});

		whitePassButton.setMaximumSize(new Dimension(150, 30));

		// botao das pretas
		JButton blackPassButton = new JButton("Pretas jogam");
		blackPassButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				timer.stop();

				whiteTurn = false;

				blackLabel.setForeground(Color.RED);
				whiteLabel.setForeground(Color.BLACK);

				timer.start();
			}
		});

		blackPassButton.setMaximumSize(new Dimension(150, 30));

		timerThread = new TimerThread(this);
		timerThread.start();

		timer = new Timer(1000, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (whiteTurn) {
					if (isWhiteTurn()) {
						whiteTime--;
						whiteLabel.setText(getTime(whiteTime));
					}
				} else {
					if (!isWhiteTurn()) {
						blackTime--;
						blackLabel.setText(getTime(blackTime));
					}
				}
			}
		});
		timer.start();

		setSize(500, 500);
		setVisible(true);

		JButton pauseButton = new JButton("Pausar");

		pauseButton.addActionListener(new ActionListener() {
			private boolean paused = false;

			public void actionPerformed(ActionEvent e) {
				pauseButton.setPreferredSize(new Dimension(30, 30));
				if (paused) {

					timer.start();
					timerThread.resumeClock();
					// atualizar botão
					pauseButton.setText("Pausar");
					paused = false;
				} else {

					timer.stop();
					timerThread.pauseClock();
					// atualizar o botão
					pauseButton.setText("Retomar");
					paused = true;
				}

			}

		});

		pauseButton.setMaximumSize(new Dimension(100, 30));

		JButton resetButton = new JButton("Reiniciar");

		resetButton.setMaximumSize(new Dimension(100, 30));
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				whiteTime = 600;
				blackTime = 600;
				whiteLabel.setText(getTime(whiteTime));
				blackLabel.setText(getTime(blackTime));

			}
		});
		// painel para organizar os botoes em 2x2
		JPanel buttonPanel = new JPanel(new GridLayout(2, 2));
		buttonPanel.add(whitePassButton);
		buttonPanel.add(blackPassButton);
		buttonPanel.add(pauseButton);
		buttonPanel.add(resetButton);
		add(buttonPanel, BorderLayout.CENTER);

	}

	boolean isWhiteTurn() {
		return whiteTurn;
	}

	String getTime(int timeInSeconds) {
		int minutes = timeInSeconds / 60;
		int seconds = timeInSeconds % 60;
		return String.format("%02d:%02d", minutes, seconds);
	}

	public JLabel getBlackLabel() {

		return blackLabel;
	}

	public JLabel getWhiteLabel() {

		return whiteLabel;
	}

	public int getBlackTime() {

		return blackTime;
	}

	public int getWhiteTime() {

		return whiteTime;
	}

}
