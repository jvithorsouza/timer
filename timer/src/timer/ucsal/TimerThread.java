package timer.ucsal;

public class TimerThread extends Thread {
	private ChessTimer clock;
	private boolean paused = false;

	public TimerThread(ChessTimer clock) {
		this.clock = clock;
	}

	@Override
	public void run() {
		synchronized (clock) {
			while (true) {
				try {
					if (paused) {
						clock.wait();
					}
					if (clock.isWhiteTurn()) {
						clock.getBlackLabel().setText(clock.getBlackLabel().getText());
						clock.getWhiteLabel().setText(clock.getTime(clock.getWhiteTime()));
						clock.notify();
						clock.wait();
					} else {
						clock.getWhiteLabel().setText(clock.getWhiteLabel().getText());
						clock.getBlackLabel().setText(clock.getTime(clock.getBlackTime()));
						clock.notify();
						clock.wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					break;
				}
			}
		}
	}

	public void pauseClock() {
		paused = true;
	}

	public void resumeClock() {
		paused = false;
		synchronized (clock) {
			clock.notify();
		}
	}

}
