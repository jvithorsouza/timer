# Timer


## Name
Timer para xadrez.

## Description
uma aplicação java simples com a função de um relógio de xadrez, que trás as funções de: jogadas das peças brancas e pretas,
além de opções de reiniciar, pausar e retomar.

## Installation
Após realizar o dowload dos arquivos, basta acessar a classe application.java e rodar o projeto.



## Authors 
João VithorSouza  e Yan Batista.


## Project status
Em desenvolvimento.

